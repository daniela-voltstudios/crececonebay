/*angular.module('listingCtrl', ['userSrvc', 'angular-mandrill'])*/
angular.module('listingCtrl', ['userSrvc'])

.controller('ListingController', ['$location', '$scope', '$http', 'UserService', '__env',
function ListingController($location, $scope, $http, UserService, __env){
	
	$scope.init = function(){
		
		if(UserService.email)
		{
		 	$scope.email = UserService.email;
		 	$scope.confirmar_email = UserService.email;
		 	$scope.name =UserService.nombre;
		 	$scope.apellido = UserService.apellido;
		 	$scope.lada= UserService.lada;
		 	$scope.phone = UserService.telefono;
		 	$scope.pais = UserService.pais;
		 	
		 	//Set phone to fijo or móvil
		 	if(UserService.tipo_tel == "fijo"){
		 		$scope.fijo = true;
		 	}else{
		 		$scope.movil =true;
		 	}
		 	
		 	
		 }
	
	  //Initialize multiselect
	  $('#country').multiselect({
    	includeSelectAllOption: true,
    	enableFiltering: true
    	
    });
    
    //Initialize popovers that appear on page
		$('a[rel=popover]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'right',
		  content: function(){return '<img src="'+$(this).data('img') + '" class="img-box" />';}
		});
		
		
  
	};
	
	
	$scope.number = "[0-9]+[\\.]?[0-9]*";
	$scope.xml = '';
	$scope.list = [];
	$scope.head = '<Leads>';
	$scope.foot = '</Leads>';
	$scope.no = 1;
	$scope.authtoken = ''; //TODO: change this to a environment variable
	$scope.text = 'hello';
	$scope.scope = 'crmapi'; //Specify Zoho API
	$scope.duplicateCheck = '1'; //Specify check duplicate records on Zoho. '1' not to update duplicated record with same email field, '2' to update a record with the same email field.
	$scope.title = '';
	$scope.description = '';
	$scope.image = '';
	$scope.condition ='';
	$scope.quantity ='';
	$scope.brand = '';
	$scope.model ='';
	$scope.size ='';
	$scope.weight='';
	$scope.countryOfManufacture= '';
	$scope.country = '';
	$scope.email ='';	
	$scope.price='';
	$scope.fin = "";
	$scope.confirmar_email="";	
	$scope.name="";
	$scope.apellido="";
	$scope.fijo="";
	$scope.movil="";
	$scope.pais="";
	$scope.lada="";
	$scope.phone="";
	
	var countryList = [];
	var added = false;
	var counter=1;
	
  var key = '';
  var iv = '';
  var cipher = '';
			
	var reader = new FileReader();
	
	reader.onload = function(event) {
	};
	
	reader.onerror = function(event) {
	    console.error("File could not be read! Code " + event.target.error.code);
	};
	
	$scope.checkMovil= function(){
		if($scope.movil.isChecked){
			$scope.fijo.isChecked = false;
		}
	}
	
	$scope.checkFijo= function(){
		if($scope.fijo.isChecked){
				$scope.movil.isChecked = false;
			} 
	}
		

  $scope.sendListingData = function() {
  	
  	$scope.authtoken = window.window.__env.auth;
  	
		if($scope.email != $scope.confirmar_email){
				alert("Los emails no coinciden");
			}else{
					if($scope.fijo.length>0){
						UserService.tipo_tel = $scope.fijo;
					var tel = $scope.fijo;
				}else{
					var tel = $scope.movil;
					UserService.tipo_tel = $scope.movil;	
				}
		
	//If the user inputted a phone number and a lada, we combine them to make the full phone number	
	 if($scope.phone != ""){
			$scope.phone = $scope.lada.toString() + $scope.phone.toString();
			parseInt($scope.phone);
		}		
		
		/*Add countries*/
		added=false;
		//console.log("lista: "+countryList);
		
		for(var i=0;i<=countryList.length+1;i++){
			
			//We have added this country, set added to true
			if($scope.country==countryList[i]){
				added=true;
				
			}
		}
		
		//If the country hasn't been selected before, we add it to our array
		if(!added){
			if($scope.country == ""){
				//alert("Selecciona un país");
			}else{
				countryList.push($scope.country);
				counter++;
			}
			
		}
		
		/*End of adding countries*/
		
		var countries = countryList.join("\n,");
		if($scope.title == ""){
			$scope.title = "Producto " + $scope.email;
		}
		
		//The user didn't input a phone number, we send to zoho a zero
		if(tel != "movil" && tel != "fijo"){
			tel = 0;
		}
		
		//The user didn't choose any countries, we send to zoho "-"
		if($scope.country == null){
			$scope.country = "-";
		}
		
		//The user didn't input one or more of the following things, we send to zoho an empty string, a zero or - depending of the field
		if($scope.phone == null){
			$scope.phone = 0;
		}
		if($scope.description == null){
					$scope.description = "-";
		}
		if($scope.condition == null){
					$scope.condition;
		}
		if($scope.quantity == null){
					$scope.quantity = "-";
		}
		if($scope.brand == null){
					$scope.brand = "";
		}
		if($scope.model == null){
					$scope.model = "";
		}
		if($scope.weight == null){
					$scope.weight = 0;
		}
		if($scope.long == null){
					$scope.long = 0;
		}
		if($scope.size == null){
					$scope.size = 0;
		}
		if($scope.height == null){
					$scope.height = 0;
		}
		if($scope.countryOfManufacture == null){
					$scope.countryOfManufacture = "";
		}
		if(countries == ""){
					countries = "";
		}
	
		/*Build the xml that's gonna be sent to zoho*/	
		
		$scope.xml = '<Products>'+
						'<row no="1">'+
							'<FL val="Product Name">'+ $scope.title + '</FL>'+
							'<FL val="Condition">'+ $scope.condition + '</FL>'+
							'<FL val="Quantity In Stock">'+ $scope.quantity + '</FL>'+
							'<FL val="Brand">'+ $scope.brand + '</FL>'+
							'<FL val="Model">'+ $scope.model + '</FL>'+
							'<FL val="Long">'+ $scope.long + '</FL>'+
							'<FL val="Width">'+ $scope.width + '</FL>'+
							'<FL val="Height">'+ $scope.height + '</FL>'+
							'<FL val="Weight">'+ $scope.weight + '</FL>'+
							'<FL val="Country Manufacture">'+ $scope.countryOfManufacture + '</FL>'+
							'<FL val="Countries to Ship To">'+ countries + '</FL>'+
							'<FL val="Unit Price">' + $scope.price + '</FL>'+
							'<FL val="Description">'+ $scope.description + '</FL>'+
							'<FL val="Product Seller">' + $scope.email + '</FL>'+
							'<FL val="Name">' + $scope.name + '</FL>'+
							'<FL val="Last Name">' + $scope.apellido + '</FL>' +
							'<FL val="Email">' + $scope.email + '</FL>'+
							'<FL val="Country">' + $scope.pais + '</FL>'+
							'<FL val="Phone">' + $scope.phone + '</FL>'+
							'<FL val="Phone Type">' + tel + '</FL>'+
							
						'</row>'+
					 '</Products>';
					 
		/*End of building xml*/			 
					 
		/*Sending data to crm*/			 
		$http({
			    method: "POST",
				  url: 'https://crm.zoho.com/crm/private/xml/Products/insertRecords?newFormat=1&authtoken='+$scope.authtoken+'&scope='+$scope.scope+'&duplicateCheck='+$scope.duplicateCheck+'&version=4&xmlData='+$scope.xml,
				  headers: {
			    	'Content-Type': 'text/plain',
			    	'Content-Type': 'application/x-www-form-urlencoded',
			    },
			  }).success(function(data, status, headers, config) {
			  		$('#modalMsg').modal('toggle');
			
			  }).error(function(data, status, headers, config) {
			  	$('#modalMsg').modal('toggle');
			  	
		 });
		 /*end of sending data to crm*/
		
	};
	
	/*Redirectioning methods*/
	
	$scope.sendToMain = function(){
		$location.path("/");
	}
	
	$scope.sendToListing = function(){
			$scope.email = UserService.email;
		 	$scope.confirmar_email = UserService.email;
		 	$scope.name =UserService.nombre;
		 	$scope.apellido = UserService.apellido;
		 	$scope.lada= UserService.lada;
		 	$scope.phone = UserService.telefono;
		 	$scope.pais = UserService.pais;
		  $location.path("/anunciar");
		
	}
	
  	
 }
}]);

