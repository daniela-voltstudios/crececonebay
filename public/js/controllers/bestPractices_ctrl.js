angular.module('bestPracticesCtrl', ['userSrvc'])

.controller('BestPracticesController', ['$location', '$scope', '$http', 'UserService', 
function BestPracticesController($location, $scope, $http, UserService){
	
	this.init = function(){
		$('body').append('<img width="1" height="1" src="//pixel.mathtag.com/event/js?mt_id=1227677&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=">');
	};

	$(document).ready(function(){
		$('body').append('<img width="1" height="1" style="display: none;" src="//pixel.mathtag.com/event/js?mt_id=1227677&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=">');
		
		
		/*pixel taken from the web*/
		
		try {
		      var bs = document.createElement("img");
		      bs.src = "https://u3s.mathtag.com/sync/img?adv=195682&uuid=55455396-1e43-4fee-b7d1-854205a31d61&mt_id=1227677";
		      bs.style.display = 'none';
		      if (document.body)
		         document.body.appendChild(bs);

		//used to sync advertiser without leaking referer to final destination
		(function() {
		    try {
			var frm = document.createElement('iframe');
			frm.style.visibility = 'hidden';
			frm.style.display = 'none';
			frm.src = "https://pixel.mathtag.com/sync/iframe?mt_uuid=55455396-1e43-4fee-b7d1-854205a31d61&no_iframe=1&mt_adid=195682";
			frm.setAttribute("id", "mm_sync_back_ground");
			var trys = 0;
		        var interval = setInterval(function(){
		            if (trys++ < 20 && interval && !document.getElementById("mm_sync_back_ground")) {
		                if (document.body) {
		                    if (interval) {
		                        clearInterval(interval);
		                        interval = 0;
		                    }
		                    document.body.appendChild(frm);
		                }
		            }
		        }, 100);
		    }
		    catch(ex)
		    {
			document.createElement("img").src="//pixel.mathtag.com/error/img?error_domain=synciframe&what="+encodeURIComponent(ex.message);
		    }
		})();
		
		}
		catch(ex)
		{
		   document.createElement("img").src="//pixel.mathtag.com/error/img?error_domain=wrap&what="+encodeURIComponent(ex.message);
		}
		
	});	
	
	$(window).load(function() {
	 
	    var pixel = document.createElement("IMG");
	    pixel.setAttribute("src", "https://pixel.mathtag.com/event/js?mt_id=1227677&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=");
	    pixel.setAttribute("height", "1");
	    pixel.setAttribute("width", "1");
	    pixel.setAttribute("style","display: none;");
	    //pixel.setAttribute("onerror", "this.onerror=null;this.src='https://a248.e.example.net/9e0d7c940412d5badab847b855f8ac46.com/conv/'+this.src.substring(this.src.indexOf('?'));");
	    document.body.appendChild(pixel);
	 
	 
	});

}]);