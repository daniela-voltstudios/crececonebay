angular.module('whyeBayCtrl', ['userSrvc'])

.controller('WhyeBayController', ['$location', '$scope', 'UserService', 
function WhyeBayController($location, $scope, $http, UserService, __env){

	$(document).ready(function(){
		$('body').append('<img width="1" height="1" style="display: none;" src="//pixel.mathtag.com/event/js?mt_id=1227680&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=">');
	});	
	
	$(window).load(function() {
	 
	    var pixel = document.createElement("IMG");
	    pixel.setAttribute("src", "https://pixel.mathtag.com/event/js?mt_id=1227680&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=");
	    pixel.setAttribute("height", "1");
	    pixel.setAttribute("width", "1");
	    pixel.setAttribute("style","display: none;");
	    //pixel.setAttribute("onerror", "this.onerror=null;this.src='https://a248.e.example.net/9e0d7c940412d5badab847b855f8ac46.com/conv/'+this.src.substring(this.src.indexOf('?'));");
	    document.body.appendChild(pixel);
	 
	 
	});

	
	$scope.init = function(){
		
		$('a[rel=popover]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'right',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Anillos</li><li>Relojes de colección</li><li>Joyería antigua</li></ol>';}
		});
		
		$('a[rel=popover2]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'right',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>De autos</li><li>De motocicletas</li><li>Vintage</li></ol>';}
		});
		$('a[rel=popover3]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'right',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Calzado hombre</li><li>Calzado mujer</li><li>Ropa típica</li></ol>';}
		});
		$('a[rel=popover4]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'left',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Artículos mejora del hogar</li><li>Muebles pequeños</li><li>Artículos de decoración</li></ol>';}
		});
		$('a[rel=popover5]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'left',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Smartphones</li><li>Tablets</li><li>Videojuegos</li></ol>';}
		});
		$('a[rel=popover6]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'left',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Monedas</li><li>Billetes</li><li>Cómics</li></ol>';}
		});
		$('a[rel=popover7]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'right',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Platería</li><li>Ropa</li><li>Pinturas</li></ol>';}
		});
		$('a[rel=popover8]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'left',
		  content: function(){return '<p class="text-blue text-popover">Te recomendamos vender:<br><ol class="text-blue text-popover"><li>Materiales educativos</li><li>Revistas</li><li>Libros de colección</li></ol>';}
		});
		
		  /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#video").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#video").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#myModal").on('show.bs.modal', function(){
        $("#video").attr('src', url+"?autoplay=1");
        //$("#video iframe").attr("src", url+"?autoplay=1");
    });
  	
  	  /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url2 = $("#video2").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#modalVid2").on('hide.bs.modal', function(){
        $("#video2").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#modalVid2").on('show.bs.modal', function(){
        //$("#video2").attr('src', url+"?autoplay=1");
        $("#video2 iframe").attr("src", url+"?autoplay=1");
    });
  	
  	  /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url3 = $("#video3").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#modalVid3").on('hide.bs.modal', function(){
        $("#video3").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#modalVid3").on('show.bs.modal', function(){
        //$("#video3").attr('src', url+"?autoplay=1");
        $("#video3 iframe").attr("src", url+"?autoplay=1");
    });
  	
  	
	};
}]);