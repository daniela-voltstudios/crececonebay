angular.module('shippingCtrl', ['userSrvc'])

.controller('ShippingController', ['$location', '$scope', '$http', 'UserService', '__env',
function ListingController($location, $scope, $http, UserService, __env){
	
	var weightList = [];
	var paisList = [];
	var tipoList = [];
	var tamanioList = [];
	
	$(document).ready(function(){
		$('body').append('<img width="1" height="1" style="display: none;" src="//pixel.mathtag.com/event/js?mt_id=1227675&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=">');
	});	
	
	$(window).load(function() {
	 
	    var pixel = document.createElement("IMG");
	    pixel.setAttribute("src", "https://pixel.mathtag.com/event/js?mt_id=1227675&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=");
	    pixel.setAttribute("height", "1");
	    pixel.setAttribute("width", "1");
	    pixel.setAttribute("style","display: none;");
	    //pixel.setAttribute("onerror", "this.onerror=null;this.src='https://a248.e.example.net/9e0d7c940412d5badab847b855f8ac46.com/conv/'+this.src.substring(this.src.indexOf('?'));");
	    document.body.appendChild(pixel);
	 
	 
	});


	
	$scope.init = function(){
			$('body').append('<img width="1" height="1" src="//pixel.mathtag.com/event/js?mt_id=1227675&mt_adid=195682&mt_exem=&mt_excl=&v1=&v2=&v3=&s1=&s2=&s3=">');
		
		var params = window.location.search.substring(1);
		UserService.utm = params;
  	
  	$('a[rel=popover]').popover({
		  html: true,
		  trigger: 'hover',
		  /*placement: 'right',*/
		  content: function(){return '<img src="'+$(this).data('img') + '" class="img-box2" />';}
		});
	};

	$scope.xml = '';
							
	$scope.list = [];
	$scope.head = '<Leads>';
	$scope.foot = '</Leads>';
	$scope.no = 1;
	$scope.authtoken = ''; //TODO: change this to a environment variable
	$scope.text = 'hello';
	$scope.scope = 'crmapi'; //Specify Zoho API
	$scope.duplicateCheck = '1'; //Specify check duplicate records on Zoho. '1' not to update duplicated record with same email field, '2' to update a record with the same email field.
	$scope.title = '';
	$scope.description = '';
	$scope.image = '';
	$scope.condition ='';
	$scope.weight='';
	$scope.country = '';
	$scope.email ='';	
	$scope.fin = "";
	$scope.name= '';
	$scope.apellido = '';
	$scope.confirmar_email='';
	$scope.fijo='';
	$scope.movil = '';
	$scope.pais= '';
	$scope.lada='';
	$scope.phone= '';
	$scope.tipo = '';
	$scope.tamanio = '';
	$scope.zip = '';
	$scope.tipo_envio = '';
	$scope.country ='';
	$scope.checkMovil= function(){
		if($scope.movil.isChecked){
			$scope.fijo.isChecked = false;
		}
	}
	
	$scope.checkFijo= function(){
		if($scope.fijo.isChecked){
				$scope.movil.isChecked = false;
			} 
	}
	
	$scope.sendShippingData = function() {
			var utm = UserService.utm;
			
			if($scope.email != $scope.confirmar_email){
				alert("Los emails no coinciden");
			}else{
					if($scope.fijo.length>0){
					var tel = $scope.fijo;
				}else{
					var tel = $scope.movil;
				}
				
				if($scope.title == ''){
					$scope.title = "Artículo " + $scope.name;
				}
				if($scope.phone != null){
					$scope.phone = $scope.lada.toString() + $scope.phone.toString();
				parseInt($scope.phone);
				}
			
				$scope.authtoken = window.window.__env.auth;
				$scope.xml = '<CustomModule1>'+
						'<row no="1">'+
							'<FL val="Product Name">'+ $scope.title + '</FL>'+
							'<FL val="CustomModule1 Name">'+ $scope.title + '</FL>'+
							'<FL val="Last name">'+ $scope.apellido + '</FL>'+
							'<FL val="Name">'+ $scope.name + '</FL>'+
							'<FL val="Shipping Name">'+ $scope.title + '</FL>'+
							'<FL val="Shipping type">'+ $scope.tipo_envio + '</FL>'+			
						    '<FL val="Country to ship">'+ $scope.country + '</FL>'+
							'<FL val="Email">'+ $scope.email + '</FL>'+
							'<FL val="Phone type">'+ tel + '</FL>'+
							'<FL val="Ph country">'+ $scope.pais + '</FL>'+
							'<FL val="Phone">'+ $scope.phone + '</FL>'+
							'<FL val="Weight">'+ weightList + '</FL>'+
							'<FL val="Package type">'+ $scope.tipo + '</FL>'+
							'<FL val="UTM">' + utm + '</FL>' +
							
						'</row>'+
					 '</CustomModule1>';
		$http({
			    method: "POST",
				  url: 'https://crm.zoho.com/crm/private/xml/CustomModule1/insertRecords?newFormat=1&authtoken='+$scope.authtoken+'&scope='+$scope.scope+'&duplicateCheck='+$scope.duplicateCheck+'&version=4&xmlData='+$scope.xml,
			     headers: {
			    	'Content-Type': 'text/plain',
			    	'Content-Type': 'application/x-www-form-urlencoded',
			    },
			  }).success(function(data, status, headers, config) {
			    $('#modalMsg').modal('toggle');
			  }).error(function(data, status, headers, config) {
			 		$('#modalMsg').modal('toggle');
			 });
			}

	};
	
	$scope.sendToMain = function(){
		$location.path("/");
	}
	


}]);

