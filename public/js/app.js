(function (angular){
	
	/**************************************************************************
   * Set environment values
   *************************************************************************/
  
  // Default environment variables
  var __env = {};
  
  // Import variables if present
  if(window){
    //Object.assign(__env, window.__env);
  }
  
	var ngModule = angular.module('ebayApp', 
		['ngRoute', 'mainCtrl', 'inicioCtrl', 'listingCtrl', 'headerDrctv', 'footerDrctv', 'guia1Ctrl', 'headerGuideDrctv',
		'guia2Ctrl', 'guia3Ctrl', 'guia4Ctrl', 'messageCtrl', 'shippingCtrl', 'whyeBayCtrl', 'header2Drctv',  'isteven-multi-select',
		'modalMsgCtrl', 'messageHighCtrl', 'termsCondCtrl', 'inicio_santanderCtrl', 'inicio_promexicoCtrl', 'headerSantanderDrctv',
		'headerPromexicoDrctv', 'messageNoCtrl', 'bestPracticesCtrl', 'optimalPublicationsCtrl', 'productIdentifierCtrl',
		'recommendationsCtrl', 'mobilesFirstCtrl', 'ebaySellerCtrl', 'publishingToolsCtrl', 'pictureRecommendationsCtrl'
		
		
		 ]);
	
	/**************************************************************************
   * Configure logging
   *************************************************************************/
  
  function disableLogging($logProvider){
    $logProvider.debugEnabled(__env.enableDebug);
  }
  
  disableLogging.$inject = ['$logProvider'];
  
  ngModule.config(disableLogging);
	
	ngModule.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$routeProvider
	
		.when("/", {
			templateUrl: "/partials/inicio.html",
			controller: "InicioController as inicio"
		})
		
		//Before: "/" 
		.when("/bienvenido", {
			templateUrl: "/partials/inicio.html",
			controller: "InicioController as inicio"
		})
		
		.when("/anunciar", {
			templateUrl: "/partials/listing.html",
			controller: "ListingController as listing"
		})
		
		.when("/modalMsg", {
			templateUrl: "/partials/modalMessage.html",
			controller: "ModalMessageController as modalMessage"
			
		})
		
		.when("/guia_paso1", {
			templateUrl: "/partials/guide_step1.html",
			controller: "Guia1Controller as guia1"
			
		})
		
		.when("/guia_paso2", {
			templateUrl: "/partials/guide_step2.html",
			controller: "Guia2Controller as guia2"
		})
		
		.when("/guia_paso3", {
			templateUrl: "/partials/guide_step3.html",
			controller: "Guia3Controller as guia3"
		})
		
		.when("/guia_paso4", {
			templateUrl: "/partials/guide_step4.html",
			controller: "Guia4Controller as guia4"
		})
		
		.when("/beneficios", {
			templateUrl: "/partials/inicio.html",
			controller: "InicioController as inicio"
		})
		
		.when("/crecer", {
			templateUrl: "/partials/inicio.html",
			controller: "InicioController as inicio"
		})
		
		//Link stayed the same	
			.when("/mejoresPracticas", {
			templateUrl: "/partials/bestPractices.html",
			controller: "BestPracticesController as bestPractices"
		})
		
		
		//before: enviar
		//We are going to temporarily disable this page
		.when("/envios", {
			templateUrl: "/partials/shipping.html",
			controller: "ShippingController as shipping"
		})
		
		//before: vendeEnEbay
		.when("/beneficiosEbay", {
			templateUrl: "/partials/whyeBay.html",
			controller: "WhyeBayController as whyeBay"
		})
		
		.when("/terminosCondiciones", {
			templateUrl: "/partials/termsCond.html",
			controller: "TermsCondController as termsCond"
		})
		
		//before: publicacionesOptimas
		.when("/optimizaPublicaciones", {
			templateUrl: "/partials/optimalPublications.html",
			controller: "OptimalPublicationsController as optimalPublications"
		})
		
		//Link stayed the same
		.when("/movilesPrimero", {
			templateUrl: "/partials/mobilesFirst.html",
			controller: "MobilesFirstController as mobilesFirst"
		})
		
		//Link stayed the same
		.when("/identificadores", {
			templateUrl: "/partials/productIdentifiers.html",
			controller: "ProductIdentifierController as identifiers"
		})
		
		
		.when("/recomendaciones", {
			templateUrl: "/partials/recommendations.html",
			controller: "RecommendationsController as recommendations"
		})
		
		//before: ebaySeller
		.when("/sellerHubEspanol", {
			templateUrl: "/partials/eBaySeller.html",
			controller: "EbaySellerController as ebaySeller"
		})
		
		//Link stayed the same
		
		.when("/herramientas", {
			templateUrl: "/partials/publishingTools.html",
			controller: "PublishingToolsController as publishingTools"
		})
		
		//before: recomendacionesImagenes
		.when("/mejoresFotos", {
			templateUrl: "/partials/pictureRecommendations.html",
			controller: "PictureRecommendationsController as pictureRecommendations"
		})
		
		//Previous: mensajeH
		.when("/informacionRecibida", {
	  	templateUrl: "/partials/messageHigh.html",
			controller: "MessageHighController as mensajeH"
		})
		
		//TODO: change this name too to informacionRecibida maybe with "pro" and "santander" next to them
		.when("/mensajeHPro", {
	  	templateUrl: "/partials/messageHighPro.html",
			controller: "MessageHighProController as mensajeHPro"
		})
		.when("/mensajeHSan", {
	  	templateUrl: "/partials/messageHighSan.html",
			controller: "MessageHighSanController as mensajeHSan"
		})
		
		//Will be deleted
		.when("/mensajeNo", {
	  	templateUrl: "/partials/messageNo.html",
			controller: "MessageNoController as mensajeNo"
		})
		
		//Will be deleted
		.when("/mensajeNoPro", {
	  	templateUrl: "/partials/messageNoPro.html",
			controller: "MessageNoProController as mensajeNoPro"
		})
		
		//Will be deleted
		.when("/mensajeNoSan", {
	  	templateUrl: "/partials/messageNoSan.html",
			controller: "MessageNoSanController as mensajeNoSan"
		})
		 
		/* Campaigns */
		.when("/promexico", {
			templateUrl: "/partials/inicio_promexico.html",
			controller: "InicioPromexicoController as inicio_promexico"
			
			
		})
		
		.when("/santander", {
			templateUrl: "/partials/inicio_santander.html",
			controller: "InicioSantanderController as inicio_santander"
		})
		
		//enviar
		.when("/santander/envios", {
			templateUrl: "/partials/shipping.html",
			controller: "ShippingController as shipping"
		})
		
		/* End of campaigns*/
		
		/*Analytics for crea tu cuenta button */ 
	  .when("/creatucuenta", {
			templateUrl: "/partials/inicio.html",
			controller: "InicioController as inicio"
		})
		
		
		.when("/mensaje", {
			templateUrl: "/partials/message.html",
			controller: "MessageController as message"
		})
		
		.when("/mensajeSan", {
			templateUrl: "/partials/messageSan.html",
			controller: "MessageSanController as messageSan"
		})
		
		.when("/mensajePro", {
			templateUrl: "/partials/messagePro.html",
			controller: "MessageProController as messagePro"
		});
		
		
		
		$locationProvider.html5Mode(true);
	}])
	
	.value('apiSite', "") //Select this for development
	//.value('apiSite', "") //Select this for production
	.value('accessToken', "");
	
	/**************************************************************************
   * Log bootstrap message
   *************************************************************************/
  
  function confirmBootstrap($log){
    $log.debug('Angular bootstrapped!');
  }
  
  confirmBootstrap.$inject = ['$log'];
  
  ngModule.run(confirmBootstrap);
  
  /**************************************************************************
   * Make environment available in Angular
   *************************************************************************/
  
  ngModule.constant('__env', __env);
  function logEnvironment($log, __env){
    $log.debug('Environment variables:');
    $log.debug(__env);
  }
  
  logEnvironment.$inject = ['$log', '__env'];
  
  ngModule.run(logEnvironment);
  
   /**************************************************************************
   * Define API service
   *************************************************************************/
  
  ngModule.service('api', ApiService);
  
  function ApiService($http, __env, data){
  	
    this.insertUser = function insertUser(){
      return $http
        .post(__env.apiUrl + __env.get + '?newFormat=1&authtoken=' + __env.auth + '&scope=crmapi&duplicateCheck=2&version=4&xmlData='+data)
        .then(function(response){
          return response;
        });
    };
  }
  
  ApiService.$inject = ['$http', '__env', 'data'];
  
})(angular)