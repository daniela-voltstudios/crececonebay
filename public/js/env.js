/**
 * Assign __env to the root window object.
 *
 * The goal of this file is to allow the deployment
 * process to pass in environment values into the application.
 *
 * The deployment process can overwrite this file to pass in
 * custom values:
 *
 * window.__env = window.__env || {};
 * window.__env.url = 'some-url';
 * window.__env.key = 'some-key';
 *
 * Keep the structure flat (one level of properties only) so
 * the deployment process can easily map environment keys to
 * properties.
 */

(function (window) {
  window.__env = window.__env || {};

  // API url
  // For demo purposes we fetch from local file in this plunk
  // In your application this can be a url like https://api.github.com
  window.__env.apiUrl = 'https://crm.zoho.com/crm/private/xml/Leads/';
  window.__env.set = 'insertRecords';
  window.__env.get = 'getRecords';
  window.__env.auth = 'ba725f8d0248d66fb601c57048823441';
  window.__env.mandKey = 'ft9pY75TNGz6DScWAe22Zw';
  window.__env.pw = 'cambiar_password';
  window.__env.iterations = '100';
  window.__env.bytes = '32';
  window.__env.iv = 'TkR7IApSZxLgPDVV';
  window.__env.mode = 'AES-CBC';
  window.__env.salt= '6cebfa7cc5d4fd855bea4c54b68fcc47208296d7ab03b61bfce4ef42de0d31141cdba119bbae723c1b75099136f9495cbe8a74123384797983efb0726c2fd3705e4ef8cc87697f238468fbc0b4701abd8f1e586e1cf697ae0d3c4e17ced61ce02a602aa6c1e4a050f40f4599051a4955dab5dadbb68636157896e298844621d9';
  

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = false;
}(this));