/**
* footerDrctv Module
*
* Modulo que contiene la directiva para el footer ubicado en múltiples páginas del sitio
*/
angular.module('footerDrctv', [])

.directive('ebayFooter', ['$rootScope', function($rootScope){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		controller: ['$scope', '$element', '$attrs', '$transclude', '$rootScope',
			function($scope, $element, $attrs, $transclude, $rootScope) {
				
			}],
		controllerAs: 'footer',
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: '/templates/footer.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, iElm, iAttrs, controller) {
			
		}
	};
}]);