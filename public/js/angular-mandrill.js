!function() {
    "use strict";
    angular.module("angular-mandrill", [])
}();;
!function() {
    "use strict";
    angular.module("angular-mandrill").constant("EVENTS", {}).constant("URLS", {
        base: ""
    })
}();;
!function() {
    "use strict";
    angular.module("angular-mandrill").provider("Mandrill", function() {
        var apiKey = {};
        var defaults = {};
        var baseUrl = "https://mandrillapp.com/api/1.0/";
        return {
            setApiKey: function(value) {
                apiKey = value
            },
            setDefaults: function(value) {
                defaults = value
            },
            $get: ["$http", function($http) {
                var mandrill = {};
                var config = {};
                var data = {
                    key: apiKey
                };
                /*return config.messages = {}, mandrill.messages = {}, mandrill.messages.send = function(userData, config = {}) {
                    var sendData = angular.extend({}, data, userData);
                    return $http.post(baseUrl + "messages/send.json", sendData, config)*/
                    
                    /*Changed this*/
                    return config.messages = {};
                    mandrill.messages = {};
                    config = {};
                    mandrill.messages.send = function(userData, config) {
                    var sendData = angular.extend({}, data, userData);
                    return $http.post(baseUrl + "messages/send.json", sendData, config)
                    /*End of changes*/
                    
                /*}, mandrill.messages.sendTemplate = function(userData, config = {}) {*/
                    }, mandrill.messages.sendTemplate = function(userData, config) {
                    var sendData = angular.extend({}, data, userData);
                    return $http.post(baseUrl + 'messages/send-template.json', sendData, config)
                }, mandrill
            }
            ]
        }
    })
}();;
!function() {
    "use strict";
    angular.module("angular-mandrill").run(function() {})
}();
